# Zapiski

## Ukazi v ukazni vrstici

Pojdi v mapo: cd <pot do mape>
Izpiši vsebino mape: ls
Preberi datoteko: cat <pot do datoteke>

Dodaj v git: git add <datoteke, ki jih želiš dodati>
Zapiši v lokalni git repozitorij: git commit -m "<opiši spremembe>"
Zapiši v oddaljeni git repozitorij: git push